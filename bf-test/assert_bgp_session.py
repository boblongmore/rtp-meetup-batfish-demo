#!/usr/bin/env python3

from pybatfish.client.commands import bf_set_network, bf_init_snapshot
from pybatfish.client.commands import bf_session
from pybatfish.client.asserts import assert_no_incompatible_bgp_sessions
from pybatfish.question import load_questions
from dotenv import load_dotenv
import os

# load dotenv
load_dotenv()

# get absolute path of current directory
dirname = os.path.dirname(__file__)

# define variables
network_name = "CML_network"
snapshot_name = "CML_routers"
snapshot_path = os.path.join(dirname, 'candidate_configs')

# set connection location
bf_session.host = os.getenv('batfish_host')

# initialization
bf_set_network(network_name)
bf_init_snapshot(snapshot_path, name=snapshot_name, overwrite=True)
load_questions()

# check for unestablished BGP sessions

result = assert_no_incompatible_bgp_sessions()
print(result)
