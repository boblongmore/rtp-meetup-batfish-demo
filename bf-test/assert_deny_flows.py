#!/usr/bin/env python3

from pybatfish.client.commands import bf_set_network, bf_init_snapshot
from pybatfish.client.commands import bf_session
from pybatfish.client.asserts import assert_filter_denies
from pybatfish.datamodel.flow import HeaderConstraints
from pybatfish.question import load_questions
from dotenv import load_dotenv
import os

# load dotenv
load_dotenv()

# get absolute path of current directory
dirname = os.path.dirname(__file__)

# define variables
network_name = "CML_network"
snapshot_name = "CML_routers"
snapshot_path = os.path.join(dirname, 'candidate_configs')

# set connection location
bf_session.host = os.getenv('batfish_host')

# initialization
bf_set_network(network_name)
bf_init_snapshot(snapshot_path, name=snapshot_name, overwrite=True)
load_questions()

# define the different criteria for the tests to run
http_headers = HeaderConstraints(srcIps="192.0.2.1",
                                 dstIps="192.0.4.1",
                                 applications=["HTTP"])

ip_headers_100 = HeaderConstraints(srcIps="192.0.2.0/24",
                                   dstIps="172.22.100.0/24")

ip_headers_101 = HeaderConstraints(srcIps="192.0.2.0/24",
                                   dstIps="172.22.101.0/24")

ip_headers_102 = HeaderConstraints(srcIps="192.0.2.0/24",
                                   dstIps="172.22.102.0/24")

# create list of headerconstraints
headers_list = [http_headers, ip_headers_100, ip_headers_101, ip_headers_102]

# run asssertions based on headerconstraints
for constraint in headers_list:
    assert_filter_denies('ansible_ext_acl', headers=constraint)
