#!/usr/bin/env python3

from pybatfish.client.commands import (bf_session, bf_set_network,
                                       bf_init_snapshot)
from pybatfish.question import bfq, load_questions
from dotenv import load_dotenv
import os

# load dotenv
load_dotenv()

# get absolute path of current directory
dirname = os.path.dirname(__file__)

# set variables for network and snapshots
network_name = "CML_network"
snapshot_name = "CML_routers"
snapshot_path = os.path.join(dirname, 'running_configs')

# connect to bf server
bf_session.host = os.getenv('batfish_host')

# initialize snapshots
bf_set_network(network_name)
bf_init_snapshot(snapshot_path, name=snapshot_name, overwrite=True)
load_questions()

# ask for routes
session_answer = bfq.routes().answer()
print(session_answer)
