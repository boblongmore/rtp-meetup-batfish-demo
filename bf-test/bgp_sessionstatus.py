#!/usr/bin/env python3

from pybatfish.client.commands import bf_session, bf_set_network
from pybatfish.question import bfq, load_questions
from pybatfish.client.commands import bf_init_snapshot
from dotenv import load_dotenv
import os

# load dotenv
load_dotenv()

# get absolute path of current directory
dirname = os.path.dirname(__file__)

# set network name and snapshot path"
network_name = "CML_network"
snapshot_name = "CML_routers"
snapshot_path = os.path.join(dirname, 'running_configs')

# connecting to localhost
bf_session.host = os.getenv('batfish_host')

# initialization process
bf_set_network(network_name)
bf_init_snapshot(snapshot_path, name=snapshot_name, overwrite=True)
load_questions()

# finding the analyzed bgp connection status
session_answer = bfq.bgpSessionStatus().answer()
print(session_answer)
