#!/usr/bin/env python3

from pybatfish.client.commands import (bf_session, bf_init_snapshot,
                                       bf_set_network)
from pybatfish.question import bfq, load_questions
from dotenv import load_dotenv
import os

# load dotenv
load_dotenv()

# get absolute path of current directory
dirname = os.path.dirname(__file__)

# set variables
network_name = "CML_network"
snapshot_name = "CML_routers"
snapshot_path = os.path.join(dirname, 'running_configs')

# connect to bf server
bf_session.host = os.getenv('batfish_host')

# initialize network snapshots and questions
bf_set_network(network_name)
bf_init_snapshot(snapshot_path, name=snapshot_name, overwrite=True)
load_questions()

# ensure the configs have been converted into snapshots
parse_status = bfq.fileParseStatus().answer().frame()
print(parse_status)

# test for line reachability
aclAns = bfq.filterLineReachability(nodes="r3-csr").answer()
# print(aclAns)
unr = aclAns.frame().sort_values(by="Unreachable_Line")
print(unr)
