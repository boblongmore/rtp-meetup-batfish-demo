# Rtp Meetup Batfish Demo

Batfish is an open source tool that does analysis of router configurations. It can analyze current state of network infrastructure, or you can use it to verify proposed changes to infrastructure. You can use its functionality through Python or Ansible.

It has the capability to analyze system information, routing and forwarding functions, and ACLs. In this presentation, we will discuss how to use batfish. We will demonstrate consuming batfish capabilities via python and Ansible. Lastly, we will tie it all together using Gitlab as our version control and our CI pipeline tool. We will check in changes to our ACL and the Gitlab CI/CD pipeline will perform an Ansible-lint, it will check out proposed changes to make sure we've not introduced any unreachable lines in the ACL and make sure that it still denies certain traffic. If those tests pass, our pipeline will push those changes to our router.

### Getting started
To get started with batfish we must install the batfish container and then the pybatfish library. 

Setup instructions for Batfish are here. https://batfish.readthedocs.io/en/latest/getting_started.html

~~~
pip install pybatfish

docker pull batfish/allinone
docker run --name batfish -v batfish-data:/data -p 8888:8888 -p 9997:9997 -p 9996:9996 batfish/allinone
~~~

To use with Ansible, we must install the ansible role.

~~~
ansible-galaxy install batfish.base
~~~

Batfish creates snapshots from device configs. They need to be referenced when running batfish. We'll backup our router configs to start.

~~~
ansible-playbook backup_configs.yml
~~~

We can then start testing against those configs with our Ansible modules. First we will see if a flow entering our 'rtr-core' device on 'GigabitEthernet1' can reach the IP of '172.22.101.10'

~~~
ansible-playbook batfish-route.yml
~~~

Next we will use the assert module again to test if there are unreachable lines in the 'rtr-core' device and if it is denying 'http' traffic.

~~~
ansible-playbook batfish-acl.yml
~~~

### Consuming batfish with Python

Next we will do a similar test to see if certain traffic is denied from the ACL on rtr-core. Batfish analyzes the configs and will tell if specified traffic is allowed or denied.

~~~ 
python3 bf-test/deny_flows.py 
~~~

Next we will look for unreachable lines.

~~~
python3 bf-test/line_reachability_local.py 
~~~

Finally, we will look at some of the questions we can ask batfish about forwarding and BGP.

~~~
python3 bf-test/bgp_sessionstatus.py 

python3 bf-test/route_q.py 
~~~

### Building an Infrastructure-as-code pipeline using Gitlab, Batfish, and Ansible

1. edit ACL
2. Push changes to Gitlab
3. Gitlab performs static linting checks as part of the linting stage
4. batfish constructs a mock ACL as part of the build-acl state
5. batfish runs reachability tests and verifys the ACL will block specific flows as part of the acl-test stage
6. Upon passing the previous stages, our pipeline will allow Ansible to push the ACL changes to rtr-core as part of the deploy-router-acl stage.

The linting stage is performed on a Gitlab shared runner.

The subsequent stages are performed on a remote runner.

(will need the ios ansible collection to use ios modules)